#import "AppDelegate.h"
#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import "hcnetsdk.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
    RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                     moduleName:@"HDLSmartHospitality"
                                              initialProperties:
                                              @{
                                                  @"language":[NSNumber numberWithInt : 1 ]
                                               }
                            ];


  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  
  [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
  NET_DVR_Init();
  
  return YES;
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
  
    NSString *bundlePath = [docDir stringByAppendingPathComponent:@"/bundle/last.bundle"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if([fileManager fileExistsAtPath:bundlePath])
    {
      //NSLog(@"返回网络Bundle");
      return [NSURL URLWithString:[docDir stringByAppendingString:@"/bundle/last.bundle"]];
    }
    else
    {
      //NSLog(@"本地Bundle");
      return [NSURL URLWithString:@"http://192.168.10.2:8081/index.bundle?platform=ios&dev=true"];
    }
  
}

-  (void)applicationWillEnterForeground:(UIApplication *)application
{
    
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{

}
- (void)applicationDidEnterBackground:(UIApplication *)application
{

}
-  (void)applicationWillTerminate:(UIApplication *)application
{
  NET_DVR_Cleanup();
  //NSLog(@"应用程序将要退出，通常用于保存数据和一些退出前的清理工作");
}

-  (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
  NET_DVR_Cleanup();
  //NSLog(@"系统内存不足，需要进行清理工作");
}

@end
