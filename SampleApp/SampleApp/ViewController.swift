//
//  ViewController.swift
//  SampleApp
//
//  Created by 汪志 on 2021/12/2.
//

import UIKit
import React

class ViewController: UIViewController {
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var containerView: UIView!
    
    
    static var ROOM_TYPE = ""
    
    var collectionData = ["Home", "A/C", "Light", "TCP"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeMenu()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadAlertController()
    }
    
    private func loadAlertController() {
        let refreshAlert = UIAlertController(title: "Refresh", message: "Double or Single room?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Double", style: .default, handler: { (action: UIAlertAction!) in
            ViewController.ROOM_TYPE = "double";
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Single", style: .cancel, handler: { (action: UIAlertAction!) in
            ViewController.ROOM_TYPE = "single";
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    private func initializeMenu() {
        let width = (view.frame.size.width - 10 - 30) / 4
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: 20)
    }
    let configSingle="{\"gwmac\":\"12:07:22:00:22:14\",\"gwIP\":\"192.168.1.255\",\"light\":{\"room\":[{\"name\":\"MainArea\",\"row\":[{\"col\":[{\"id\":1}]},{\"col\":[{\"id\":2},{\"id\":3},{\"id\":4}]}]},{\"name\":\"BathRoom\",\"row\":[{\"col\":[{\"id\":8},{\"id\":9}]}]}]},\"air\":{\"row\":[{\"aid\":10}]},\"device\":{\"1\":{\"id\":1,\"tid\":10,\"name\":\"Master\",\"icon\":\"Master.png\",\"signtype\":0,\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":12},\"2\":{\"id\":2,\"tid\":11,\"name\":\"LeftReadingLight\",\"icon\":\"LeftReadingLight.png\",\"signtype\":0,\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":10},\"3\":{\"id\":3,\"tid\":12,\"name\":\"RightReadingLight\",\"icon\":\"RightReadingLight.png\",\"signtype\":0,\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":8},\"4\":{\"id\":4,\"tid\":13,\"name\":\"NightLight\",\"icon\":\"NightLight.png\",\"signtype\":0,\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":9},\"8\":{\"id\":8,\"tid\":14,\"name\":\"Mirror\",\"icon\":\"Mirror.png\",\"signtype\":0,\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":7},\"9\":{\"id\":9,\"tid\":14,\"name\":\"Bathroom\",\"icon\":\"Bathroom.png\",\"signtype\":0,\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":6},\"10\":{\"name\":\"livingRoom\",\"currentTemp\":{\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":21},\"power\":{\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":22},\"mode\":{\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":23},\"speed\":{\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":24},\"temperature\":{\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":25}},\"MakeupRoom\":{\"name\":\"MakeupRoom\",\"icon\":\"MakeupRoom.png\",\"signtype\":0,\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":4},\"DoNotDisturb\":{\"name\":\"DoNotDisturb\",\"icon\":\"DoNotDisturb.png\",\"signtype\":0,\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":3}}}"
    
    let configDoule="{\"gwmac\":\"12:07:22:00:22:14\",\"gwIP\":\"192.168.1.255\",\"light\":{\"room\":[{\"name\":\"LivingRoom\",\"row\":[{\"col\":[{\"id\":2}]}]},{\"name\":\"Master\",\"row\":[{\"col\":[{\"id\":1}]}]},{\"name\":\"BedRoom\",\"row\":[{\"col\":[{\"id\":4},{\"id\":5},{\"id\":6},{\"id\":7}]}]},{\"name\":\"Bathroom\",\"row\":[{\"col\":[{\"id\":8},{\"id\":9}]}]}]},\"air\":{\"row\":[{\"aid\":10},{\"aid\":11}]},\"device\":{\"1\":{\"name\":\"Master\",\"icon\":\"Master.png\",\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":12},\"2\":{\"name\":\"LivingRoom\",\"icon\":\"LivingRoom.png\",\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":14},\"4\":{\"name\":\"LeftReadingLight\",\"icon\":\"LeftReadingLight.png\",\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":10},\"5\":{\"name\":\"RightReadingLight\",\"icon\":\"RightReadingLight.png\",\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":8},\"6\":{\"name\":\"NightLight\",\"icon\":\"NightLight.png\",\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":9},\"7\":{\"name\":\"Bedroom\",\"icon\":\"Bedroom.png\",\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":12},\"8\":{\"name\":\"Mirror\",\"icon\":\"Mirror.png\",\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":7},\"9\":{\"name\":\"Bathroom\",\"icon\":\"Bathroom.png\",\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":6},\"MakeupRoom\":{\"tid\":14,\"name\":\"MakeupRoom\",\"icon\":\"MakeupRoom.png\",\"signtype\":0,\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":4},\"DoNotDisturb\":{\"tid\":14,\"name\":\"DoNotDisturb\",\"icon\":\"DoNotDisturb.png\",\"signtype\":0,\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":3},\"10\":{\"name\":\"livingRoom\",\"currentTemp\":{\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":21},\"power\":{\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":22},\"mode\":{\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":23},\"speed\":{\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":24},\"temperature\":{\"value\":\"0\",\"lid\":0,\"rcuid\":65,\"port\":25}},\"11\":{\"name\":\"bedRoom\",\"currentTemp\":{\"value\":\"0\",\"lid\":0,\"rcuid\":66,\"port\":21},\"power\":{\"value\":\"0\",\"lid\":0,\"rcuid\":66,\"port\":22},\"mode\":{\"value\":\"0\",\"lid\":0,\"rcuid\":66,\"port\":23},\"speed\":{\"value\":\"0\",\"lid\":0,\"rcuid\":66,\"port\":24},\"temperature\":{\"value\":\"0\",\"lid\":0,\"rcuid\":66,\"port\":25}}}}"
    private func moveToTVController() {
        removeContainerViews()
        
        //let initProps:[String:Any]=["isFirst":isFirst,"config":configDoule]
        let initProps:[String:Any]=["languageCode":languageCode]
        isFirst=false
        let rootView = CusReactModule.sharedInstance.viewForModule(
            "HDLSmartHospitality",
            initialProperties: initProps)
        
        let vc = UIViewController()
        vc.view = rootView
        addChild(vc)
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(vc.view)
        
        NSLayoutConstraint.activate([
            vc.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            vc.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            vc.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            vc.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
            ])
        vc.didMove(toParent: self)
    }
    
    private func removeContainerViews() {
        for view in containerView.subviews {
            view.removeFromSuperview()
        }
    }
    var isFirst=true
    var languageCode=0
    private func moveToLightController() {
        removeContainerViews()
        
        let initProps:[String:Any]=["languageCode":languageCode]
        let rootView = CusReactModule.sharedInstance.viewForModule(
            "HDLSmartHospitality",
            initialProperties: initProps)
       
        let vc = UIViewController()
        vc.view = rootView
        addChild(vc)
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(vc.view)
        
        NSLayoutConstraint.activate([
            vc.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            vc.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            vc.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            vc.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
            ])
        vc.didMove(toParent: self)
    }
    
    private func moveToACController() {
        removeContainerViews()

        let initProps:[String:Any]=["languageCode":languageCode]
        let rootView = CusReactModule.sharedInstance.viewForModule(
            "HDLSmartHospitality",
            initialProperties: initProps)

        let vc = UIViewController()
        vc.view = rootView
        addChild(vc)
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.addSubview(vc.view)
       NSLayoutConstraint.activate([
            vc.view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            vc.view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            vc.view.topAnchor.constraint(equalTo: containerView.topAnchor),
            vc.view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
            ])
        vc.didMove(toParent: self)
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionViewCell", for: indexPath)
        if let label = cell.viewWithTag(100) as? UIButton {
            
            label.setTitle(collectionData[indexPath.row],for: .normal)
            label.addTarget(self, action: #selector(onClick(sender:)), for: .touchUpInside)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //let selectedItem = collectionData[indexPath.row]
        
    }
    
    @objc func onClick(sender: UIButton) {
        let selectedItem = sender.title(for: .normal)
        
        if (selectedItem == "TCP") {
            moveToTVController()
        } else if (selectedItem == "A/C") {
            moveToACController()
        } else if (selectedItem == "Light") {
            moveToLightController()
        }
    }
}
