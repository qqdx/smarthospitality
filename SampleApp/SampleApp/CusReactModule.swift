//
//  CusReactModule.swift
//  SampleApp
//
//  Created by 汪志 on 2021/12/2.
//

import Foundation
import React

class CusReactModule: NSObject {
    static let sharedInstance = CusReactModule()
    var bridge: RCTBridge?
    
    func createBridgeIfNeeded() -> RCTBridge {
        if bridge == nil {
            bridge = RCTBridge.init(delegate: self, launchOptions: nil)
        }
        return bridge!
    }
    
    func viewForModule(_ moduleName: String, initialProperties: [String : Any]?) -> RCTRootView {
        let viewBridge = createBridgeIfNeeded()
        let rootView: RCTRootView = RCTRootView(
            bridge: viewBridge,
            moduleName: moduleName,
            initialProperties: initialProperties)
        
        return rootView
    }
}

extension CusReactModule: RCTBridgeDelegate {
    func sourceURL(for bridge: RCTBridge!) -> URL! {
        print(ViewController.ROOM_TYPE)
        
        if (ViewController.ROOM_TYPE == "double") {
            print("./double_version/double.ios.bundle")
            //return URL(string:"http://192.168.1.109:8081/index.bundle?platform=ios")
            return URL(string: "double_version/double.ios.bundle")
        } else {
            print("single_version/last.bundle")
            return URL(string: "single_version/last.bundle")
    }
        
        
        //return URL(string: "single_version/single.ios.bundle")
        
    }
}
